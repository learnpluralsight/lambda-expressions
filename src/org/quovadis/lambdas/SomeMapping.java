package org.quovadis.lambdas;

import org.quovadis.lambdas.model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class SomeMapping {
    public static void main(String[] args) {
        User sarah = new User("Sarah");
        User james = new User("James");
        User mary = new User("Mary");
        User john = new User("John");

        List<User> users = List.of(sarah, james, mary, john);
        List<String> names = new ArrayList<>();
        Function<User, String> toNameS = u -> u.getName();
        Function<User, String> toName = User::getName;

        for(User user: users)
            names.add(toName.apply(user));

        List<String> namesOf = users.stream()
                .map(toName)
                .collect(Collectors.toList());

        namesOf.forEach(System.out::println);

    }
}
