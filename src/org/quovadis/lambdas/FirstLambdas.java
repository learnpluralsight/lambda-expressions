package org.quovadis.lambdas;

import java.util.function.Consumer;
import java.util.function.Supplier;

public class FirstLambdas {
    public static void main(String[] args) {
        //Supplier
        Supplier<String> supplier = () -> {
            return "Hello!";
        };

        // Consumer
        Consumer<String> consumer = (String s) -> {
            System.out.println("I am side the Consumer!");
            System.out.println(s.concat(", Wonderful World!"));
        };
        consumer.accept(supplier.get());
    }
}
