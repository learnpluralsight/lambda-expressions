package org.quovadis.lambdas;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class MoreLambdas {
    public static void main(String[] args) {
        List<String> strings = new ArrayList<>(List.of("one", "two", "three", "four", "five"));

        strings.removeIf(s -> !s.startsWith("t"));
        strings.forEach(System.out::println);
    }
}
